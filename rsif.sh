#!/bin/sh
#
# replace string in file
#
# N.B. this has security issues if if you are concerned about other users accessing /tmp/.replaceXXXXXX
#
TMP=`mktemp /tmp/.replaceXXXXXX`
search="$1"
shift
replace="$1"
shift
while [ "$1" != "" ] 
do
cat "$1" | sedlite "$search" "$replace" > $TMP
cat $TMP > "$1"
shift
done
rm $TMP

