# sedlite

A simple stream editor ala sed, but without support for regular expressions.

Replace a string found in the input stream with another string, but without any regex support or escaping (except what the shell requires).

The aim of sedlite is to keep replacements simple so no expression syntax is supported.

This enables use of variables in scripts that may contain any characters as replacement strings.

    #> echo "Enter your desired username"
    #> read USERNAME
    #> cat template.txt | sedlite "$USERNAME" "@username@" > my.config

Achieving the same results with sed is complicated.

## OPTIONS
sedlite must be called with exactly two parameters.  No options are supported.

The first parameter is the string to search for. The second paramter is the replacement string. 

The lack of options enables replacements with strings that appear to be options.

The following will replace occurances of "-p" with "-P"

    #> cat script.sh | sedlite -p -P > newscript.sh

No character such as - or [  or * have any special meaning to sedlite, however they may have meaning to the shell.

## EXAMPLES
The following two statements are equivalent.

    #> echo "What to find in the string" | sedlite "to find" "did we find"

    #> echo "What to find in the string" | sed -e 's/to find/did we find/g'

When it is desired to replace characters used by regular expressions sedlite is more convenient.

    #> echo $HOME | sedlite /home/ /usr/lib/

    #> echo $HOME | sed -e s/\\/home\\//\\/usr\\/lib\\//g

sedlite accepts two arguments with no escaping, but your shell may need to escape or quote strings to achieve the correct results.

The following are equivalent in bash.

    #> ... | sedlite My\ Documents /home/user/
    #> ... | sedlite 'My Documents' /home/user/
    #> ... | sedlite "My Documents" /home/user/

Stripping sequences completely can be achieved by sending the empty string to as the second parameter.

    #> cat rude.doc | sedlite 'f*ck' "" > nice.doc

sedlite handles newlines like any other character, for example to strip newlines in bash pass a quoted newline character.

    #> echo "1234
    5678" | sedlite "
    " -
    1234-5678-#> 

# EXIT STATUS

sedlite returns 0 if replacements were made 1 if there were no matches found in in the stream and greater than 1 for an error.

# SEE ALSO

*rsif* - for sedlite style replacements in files.

*rsifr* - for sedlite style recursive replacements in directories.

# COPYRIGHT

Copyright (C) 2011 Teknopaul

sedlite is free software licensed under GPL.

# AUTHOR

teknopaul@everywhere  
  i.e. twitter, gmail, yahoo, irc, msn, facebook, github
http://github.com/teknopaul
http://tp23.org

