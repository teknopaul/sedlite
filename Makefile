PREFIX ?= /usr/bin

sedlitemake: sedlite.c sedlite.h
	gcc -Wall -o sedlite sedlite.c

.PHONY: clean deb install uninstall

clean:
	rm sedlite

deb:
	deploy/build-deb.sh

install:
	mkdir -p ${DESTDIR}${PREFIX} ${DESTDIR}/usr/share/man/man1
	cp sedlite ${DESTDIR}${PREFIX}
	cd ${DESTDIR}${PREFIX} && ln -s sedlite rsif
	cd ${DESTDIR}${PREFIX} && ln -s sedlite rsifr
	cp *.1 ${DESTDIR}/usr/share/man/man1

uninstall:
	rm -f /usr/bin/sedlite /usr/bin/rsif /usr/bin/rsifr
	rm -f /usr/share/man/man1/sedlite.1 /usr/share/man/man1/rsif.1 /usr/share/man/man1/rsifr.1


