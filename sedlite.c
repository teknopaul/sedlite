#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sedlite.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

/*
 * Copyright 2011 teknopaul
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *                                                                   USA
 */

/**
 * Simple search and replace.
 * 
 * Replace a string found in the input stream with another string, but without ANY regex or escaping wierdness (except what the shell requires).
 * 
 * >  echo "What to find in the string" | sedlite "to find" "did we find"
 * 
 * is equivalent to 
 * 
 * >  echo "What to find in the string" | sed -e 's/to find/did we find/g'
 * 
 * But also
 * 
 * >  echo $HOME | sedlite /home/ /usr/lib/
 * 
 * is equivalient to
 * 
 * > echo $HOME | sed -e 's/\/home\//\/usr\/lib\//g'
 * 
 * So you see why sedlite is prettier than sed in some instances.  
 * 
 * sed is powerful, sedlite is not, and never will be.
 * 
 * Now try writing the following in sed
 * 
 * cat file.txt | sedlite /usr/lib/ $HOME/lib
 * 
 * If you that was easy now try unescaping every possibility in regular expressions.
 * 
 * @author teknopaul
 * @licence GPL
 */


/**
 * The main can be called in three modes
 * 
 *   sedlite - replace stdin to stdout
 *   rsif - replace string in file (clone of )
 *   rsifr - rsif recusivly
 * 
 * It is not called with options e.g. -R  so that you can replace -R with -V and dont need to do any escaping
 * 
 */
int main(int argc, char *argv[])
{

  /* sedlite mode only stream editing stdin and write to stdout */
  // if ! length correct balk
  if ( argc < 3 ) {
    
    fprintf(stderr, "Usage: %s [search] [replace] [files...]\n", argv[0]);
    return 10;
    
  }

  // read command line args
  char *search_str = argv[1];
  char *replace_str = argv[2];
  
  if ( argc == 3) {
    
    return replace(search_str, replace_str, stdin, stdout);
    
  }
  
  /*
   * rsif and rsifr mode works on files and recursivly on files and directories 
   * 
   * Check the end of arg[0] so it may be called ./rsifr or /usr/bin/rsifr
   */
  char *bin_name = argv[0];
  int bin_len = strlen(bin_name);
  int is_rsif = 0;
  int is_rsifr = 0;
  int recurse = 0;
  if ( strcmp("rsifr", bin_name + bin_len - 5) == 0 ) {
    is_rsifr = 1;
    recurse = 1;
  }
  if ( strcmp("rsif",bin_name + bin_len - 4) == 0 ) {
    is_rsif = 1;
  }
  
  if ( (is_rsif || is_rsifr) && argc > 3) {
    
    int i, ret, changed;
    ret = 0; 
    changed = 1;
    
    for (i = 3; i < argc; i++) {
      
      char *file_name = argv[i];
      
      int ret_one = replace_file(search_str, replace_str, file_name, recurse);
      if (ret_one == 0) {
	changed = 0;
      }
      else if (ret_one > 1) {
	ret += ret_one;
      }
    }
    
    // TODO unlink the tmp file
    if (ret > 1) {
      return ret;
    }
    else {
      return changed;
    }
  }
  
  return 23;
  
};


static int recurse_dir(const char *search_str, const char *replace_str, char *dir_name)
{
  
  DIR *dir = opendir(dir_name);
  if (dir == NULL) {
      return 3;
  }
  
  int ret = 0;
  int ret_one = 0;
  struct dirent *dirp;
  while ( (dirp = readdir(dir)) != NULL ) {
    if ( strcmp(dirp->d_name, ".") == 0 || strcmp(dirp->d_name, "..") == 0 ) {
      continue;
    }
    
    // dir + "/" + name 
    char *full_path = malloc(strlen(dir_name) + strlen(dirp->d_name) + 1);
    strcat(full_path, dir_name);
    strcat(full_path, "/");
    strcat(full_path, dirp->d_name);
    
    ret_one = replace_file(search_str, replace_str, full_path, 1);
    
    free(full_path);
    if (ret_one == 1 && ret < 2) {
      ret = 1;
    }
    if (ret_one > 1) {
      ret += 2;
    }
  }
  
  return ret;
};
/**
 * @return 0 == ok, 1 == no changes found, > 2 is an error
 */
int replace_file(const char *search_str, const char *replace_str, char *file_name, int recurse)
{
  int ret, // return code
      ch,  // the character being read
      fd;  // file descriptor
  
  ret = 0;
  
  struct stat stat_s;
  int stat_ret = stat(file_name, &stat_s);
  if (stat_ret != 0) { // TODO support -R
    fprintf(stderr, "Unable to stat file %s\n", file_name);
    return 2;
  }

  if ( recurse && S_ISDIR(stat_s.st_mode) ) {
    return recurse_dir(search_str, replace_str, file_name);
  }
  
  if ( ! S_ISREG(stat_s.st_mode) ) {
    fprintf(stderr, "Not a file %s\n", file_name);
    return 2;
  }
      

  // open file to read
  FILE *in = fopen(file_name, "r+");
  if (in == NULL) {
    ret++;
    fprintf(stderr, "Unable to open %s\n", file_name);
    return 3;
  }

  // create a unique temp file
  char template[14];
    
  ret = 0;
  char write_buffer[BUFSIZ];
  memcpy(template, "replaceXXXXXX", 14);
  fd = mkstemp(template);
  if (fd == -1) {
    fprintf(stderr, "Unable to open temp file\n");
    fclose(in);
    unlink(template);
    return 4;
  }
  
  // open tmp file
  FILE *tmp = fdopen(fd, "w+");
  if (tmp == NULL) {
    fprintf(stderr, "Unable to open temp file\n");
    fclose(in);
    unlink(template);
    return 4;
  }
  
  // do the replace
  ret = replace(search_str, replace_str, in, tmp);
  fclose(in);
  if (ret == 1) { // no changes mame
    fclose(tmp);
    unlink(template);
    return 1;
  }

  rewind(tmp);

  // reopen same file for writing
  FILE *out = fopen(file_name, "w");
  if (out == NULL) {
    fprintf(stderr, "Unable to open %s\n", file_name);
    fclose(tmp);
    unlink(template);
    return 3;
  }
  
  // add write buffering
  setbuf(out, write_buffer);
  
  // write changes
  while ( (ch = fgetc(tmp)) != EOF  ) {
    fputc(ch, out);
  }
  fflush(out);
  
  // close up
  fclose(out);
  fclose(tmp);
  unlink(template);
  return ret;
  
};

/**
 * @return 0 if changed 1 if the search string was not found at least once 
 */
int replace(const char *search_str, const char *replace_str, FILE *in, FILE *out)
{

  // set up buffer of at least search_len chars for matching
  int buf_pos = 0; // position in the buffer we are writing too
  int search_len = strlen(search_str);
  int buffer_len = search_len + BUFFER_LEN;
  char *buffer = malloc( sizeof(char) * buffer_len);
  
  if (buffer == NULL) {
    return 2;
  }
  
  memset(buffer, 0, buffer_len);

  int ch, i, changed;
  
  // search input for the match
  for( i = 0, changed = 1; (ch = fgetc(in)) != EOF ;i++ ) {
    
    buffer[buf_pos++] = ch;
//    if ( buf_pos < search_len - 1) {
//      continue;
//    }
    // if match spit replace & skip search
    if ( strncmp(&buffer[buf_pos - search_len], search_str, search_len) == 0 ) {
      changed = 0;
      fputs(replace_str, out);
      memset(buffer, 0, buffer_len);
	  buf_pos = 0;
	  continue;
    }
    // else spit first non-matched char
    else {
      if (buf_pos - search_len >= 0) putc(buffer[buf_pos - search_len], out);
    }
    
    // if we have run out of buffer copy the buffer to the start of memory and wipe the rest
    if ( buf_pos + 1 > buffer_len ) {
      memmove(buffer, &buffer[buf_pos - search_len], search_len);
      memset(&buffer[search_len], 0, buffer_len - search_len);
      buf_pos = search_len;
    }
  }
  // write the unmatched buffer 
  fputs(&buffer[++buf_pos - search_len], out);
  
  free(buffer);
  
  return changed;
  
};

